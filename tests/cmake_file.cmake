# CMake: SLOC=5 LLOC=0
cmake_minimum_required(VERSION 2.8)
project(dummy_test)

# A standard if() syntax, also cmake is case-insensitive
IF (CMAKE_C_COMPILER_ID STREQUAL "GNU")
    MESSAGE(STATUS "You have a GNU compiler!")
ENDIF ()

# Test a commented out line
# SET(VERSION "1.0.0")
