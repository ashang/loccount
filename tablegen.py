#!/usr/bin/env python3
# SPDX-License-Identifier: BSD-2-Clause

# Massage a JSON traits table into an equivalent Go initializer.
# We do this in code generation, rather than reading in JSON
# at loccount startup, to make it faster.
#
# Two options:
#
# -g   Generate a Go initializer
# -j   Dump JSON after folding in parts entries with De references resolved.

import json, sys, getopt

try:
    (options, arguments) = getopt.getopt(sys.argv[1:], "gj")
except getopt.GetoptError as e:
    print(e)
    sys.exit(1)
generate_go = False
generate_json = False

for (switch, val) in options:
    if switch == '-g':
        generate_go = True
    elif switch == '-j':
        generate_json = True

if len(arguments) == 0:
    sys.stderr.write("%s: at least one argument is required.\n" % sys.argv[0])
    sys.exit(1)

source = arguments[0]

if len(arguments) == 1:
    target = "/dev/stdout"
else:
    target = arguments[1]

parts = {}

def escapify(s):
    s = repr(s)[1:-1]
    s = s.replace(r'"', r'\"').replace(r'\'', r"'")
    return s

with open(target, "wb") as wfp:
    with open(source, "r") as rfp:
        if generate_go:
            wfp.write(b"""\
// This file is generated - do not hand-hack!

package main

var genericLanguages = []genericLanguage{
""")
        for line in rfp:
            if line[0] != "{":
                continue
            entry = json.loads(line)
            if "Ext" in entry:
                if generate_go:
                    entry["Ext"] = repr(entry["Ext"]).replace("'", '"')[1:-1]
            if "Ml" in entry and generate_go:
                entry["Ml"] = ",".join([("[2]string{\"%s\",\"%s\"}" % (escapify(pair[0]),escapify(pair[1]))) for pair in entry["Ml"]])
            else:
                entry["Ml"] = ""
            for k in ("Bcl", "Bct", "Wcl", "St"):
                if k not in entry:
                    entry[k] = ""
                else:
                    entry[k] = escapify(entry[k])
            if "Verifier" not in entry:
                entry["Verifier"] = "nil"
            # Might be a reusable part
            if "Ext" not in entry:
                parts[entry["Name"]] = entry
                continue
            elif "De" in entry:
                proto = parts[entry["De"]]
                for k in ("Bcl", "Bct", "Wcl", "St"):
                    if not entry.get(k) and k in proto:
                        entry[k] = proto[k]
                if "Quirks" not in entry:
                    entry["Quirks"] = []
                for f in proto["Quirks"]:
                    if not f in entry["Quirks"]:
                        entry["Quirks"].append(f)
            # Nope.
            if generate_go:
                if "Quirks" not in entry:
                    entry["Quirks"] = ["nf"]
                entry["Quirks"] = " | ".join(entry["Quirks"])
                out = "\t{Name:\"%(Name)s\",Ext:[]string{%(Ext)s},Bcl:\"%(Bcl)s\",Bct:\"%(Bct)s\",Wcl:\"%(Wcl)s\",quirks:%(Quirks)s,St:\"%(St)s\",Ml:[][2]string{%(Ml)s},verifier:%(Verifier)s}," % entry
            elif generate_json:
                out = json.dumps(entry)
            wfp.write((out + "\n").encode("ascii"))
        if generate_go:
            wfp.write(b"}\n")
